<?php
/*
 * ROO :: Religionsunterricht leichtgemacht
 *
 * @package Roo
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/peregrinus/roo
 * @version git: $Id$
 *
 * Roo is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/start', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});

if (!function_exists('routeNames')) {
    function routeNames($model)
    {
        $routes = [];
        foreach (['index', 'create', 'store', 'show', 'edit', 'update', 'destroy'] as $verb) {
            $routes[$verb] = "{$model}.{$verb}";
        }
        return $routes;
    }
}

// import model routes
foreach (glob(app_path('Models/*.php')) as $modelFile) {
    if (!str_contains($modelFile, 'Abstract')) {
        $modelClass = 'App\\Models\\'.pathinfo($modelFile, PATHINFO_FILENAME);
        if (method_exists($modelClass, 'registerRoutes')) {
            $modelClass::registerRoutes();
        }
    }
}

// import all other route files
foreach (glob(base_path('routes/web/*')) as $file) {
    require($file);
}
