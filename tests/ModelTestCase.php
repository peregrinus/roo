<?php
/*
 * ROO :: Religionsunterricht leichtgemacht
 *
 * @package Roo
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/peregrinus/roo
 * @version git: $Id$
 *
 * Roo is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Tests;

use App\Models\AbstractModel;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;

class ModelTestCase extends TestCase
{

    use RefreshDatabase;

    public const defaultResourceRoutes = [
        'index' => ['plural' => true, 'model' => false, 'auth' => true, 'verb' => 'GET', 'needsData' => false],
        'show' => ['plural' => false, 'model' => true, 'auth' => true, 'verb' => 'GET', 'needsData' => false],
        'create' => ['plural' => false, 'model' => false, 'auth' => true, 'verb' => 'GET', 'needsData' => false],
        'store' => ['plural' => false, 'model' => false, 'auth' => true, 'verb' => 'POST', 'needsData' => true],
        'edit' => ['plural' => false, 'model' => true, 'auth' => true, 'verb' => 'GET', 'needsData' => false],
        'update' => ['plural' => false, 'model' => true, 'auth' => true, 'verb' => 'PATCH', 'needsData' => true],
        'destroy' => ['plural' => false, 'model' => true, 'auth' => true, 'verb' => 'DELETE', 'needsData' => false],
    ];
    /** @var string Model class */
    protected $modelClass;

    /**
     * Get all routes used by this model
     */
    public function getResourceRoutes(): array
    {
        $routes = [];
        foreach (array_diff(array_keys(static::defaultResourceRoutes), ($this->modelClass)::$exceptRoutes) as $key) {
            $routes[$key] = array_values(['key' => $key, ...static::defaultResourceRoutes[$key]]);
        }
        return $routes;
    }

    /**
     * Checks if this model is a descendant of AbstractModel
     *
     * @return void
     */
    public function test_model_is_derrived_from_abstract_model()
    {
        $this->assertTrue(is_subclass_of($this->modelClass, AbstractModel::class));
    }

    /**
     * Test if model resource routes exist
     *
     * @dataProvider getResourceRoutes
     * @return void
     */
    public function test_route_exists($key, $plural, $needsModel, $auth, $verb, $needsData)
    {
        $this->assertTrue(Route::has($this->getRouteName($key, $plural)));
    }

    /**
     * Get route name
     *
     * @param  string  $key
     * @param  bool    $plural
     * @return string
     */
    protected function getRouteName(string $key, bool $plural = false): string
    {
        return ($plural ? ($this->modelClass)::pluralKey() : ($this->modelClass)::singularKey()).'.'.$key;
    }

    /**
     * Test if controller methods exist
     *
     * @dataProvider getResourceRoutes
     * @return void
     */
    public function test_controller_method_exists($key, $plural, $needsModel, $auth, $verb, $needsData)
    {
        $this->assertTrue(method_exists(($this->modelClass)::controllerClass(), $key));
    }

    /**
     * Test if model resource routes exist
     *
     * @dataProvider getResourceRoutes
     * @return void
     */
    public function test_route_reachable($key, $plural, $needsModel, $auth, $verb, $needsData)
    {
        if ($needsModel) {
            $route = route($this->getRouteName($key, $plural), $this->getFactory()->create()->id);
        } else {
            $route = $this->getRoute($key, $plural);
        }
        $verb = strtolower($verb);
        if ($auth) {
            $this->actAsUser();
        }
        if ($needsData) {
            $response = $this->$verb($route, $this->getFactory()->raw());
        } else {
            $response = $this->$verb($route);
        }
        if ($verb == 'get') {
            $response->assertStatus(200);
        } else {
            $response->assertRedirect($this->getRoute('index', true));
        }
    }

    /**
     * Get the factory for this model
     *
     * @return Factory
     */
    protected function getFactory(): Factory
    {
        return ($this->modelClass)::factory();
    }

    /**
     * Get route
     *
     * @param  string  $key
     * @param  bool    $plural
     * @return string
     */
    protected function getRoute(string $key, bool $plural = false): string
    {
        return route($this->getRouteName($key, $plural));
    }

    /**
     * Perform the following operations as a logged-in user
     *
     * @return User
     */
    protected function actAsUser(): User
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        return $user;
    }

    /**
     * Test if model resource routes exist
     *
     * @dataProvider getResourceRoutes
     * @return void
     */
    public function test_route_not_reachable_for_guest($key, $plural, $needsModel, $auth, $verb, $needsData)
    {
        if ($needsModel) {
            $route = route($this->getRouteName($key, $plural), $this->getFactory()->create());
        } else {
            $route = $this->getRoute($key, $plural);
        }
        if (!$auth) {
            $this->markTestSkipped('Route is allowed for guests');
        }
        $verb = strtolower($verb);
        if ($needsData) {
            $response = $this->$verb($route, $this->getFactory()->raw());
        } else {
            $response = $this->$verb($route);
        }
        $response->assertRedirect(route('login'));
    }

    /**
     * Test if model can be created normally
     *
     * @return void
     */
    public function test_model_can_be_created()
    {
        $model = $this->getFactory()->create();
        $this->assertEquals(1, ($this->modelClass)::count());
    }

    /**
     * Test if model can be created normally
     *
     * @return void
     */
    public function test_model_can_be_created_through_action()
    {
        $creator = app(($this->modelClass)::getContractName('create'));
        $creator->create(User::factory()->create(), $this->getFactory()->raw());

        $this->assertEquals(1, ($this->modelClass)::count());

    }

    /**
     * Test if model creation fires CreatingModel event
     *
     * @return void
     */
    public function test_model_creation_fires_event()
    {
        Event::fake();
        $creator = app(($this->modelClass)::getContractName('create'));
        $creator->create(User::factory()->create(), $this->getFactory()->raw());

        Event::assertDispatched(($this->modelClass)::getEventName('adding'));

    }

    /**
     * Test if the model can be created through the web
     *
     * @return void
     */
    public function test_model_can_be_created_via_the_web()
    {
        $attributes = $this->getFactory()->raw();
        $this->actAsUser();
        $result = $this->post($this->getRoute('store'), $attributes);
        $result->assertRedirect($this->getRoute('index', true));
        $this->assertEquals(1, ($this->modelClass)::count());
        $this->assertAllAttributesAreSetOnModel($attributes);
    }

    /**
     * Assert that all the attributes are stored in the model
     *
     * @param $attributes
     * @return void
     */
    protected function assertAllAttributesAreSetOnModel($attributes)
    {
        $model = ($this->modelClass)::first();
        foreach ($attributes as $key => $value) {
            $this->assertEquals($value, $model->$key);
        }
    }

    /**
     * Test if model can be updated normally
     *
     * @return void
     */
    public function test_model_can_be_updated()
    {
        $model = $this->getFactory()->create();
        $attributes = $this->getFactory()->raw();
        $model->update($attributes);

        $this->assertAllAttributesAreSetOnModel($attributes);
    }

    /**
     * Test if model can be updated normally
     *
     * @return void
     */
    public function test_model_can_be_updated_through_action()
    {
        $updater = app(($this->modelClass)::getContractName('update'));
        $model = $this->getFactory()->create();
        $attributes = $this->getFactory()->raw();
        $updater->update(User::factory()->create(), $model, $attributes);

        $this->assertAllAttributesAreSetOnModel($attributes);
    }

    /**
     * Test if model update fires UpdatingModel event
     *
     * @return void
     */
    public function test_model_update_fires_event()
    {
        Event::fake();
        $updater = app(($this->modelClass)::getContractName('update'));
        $model = $this->getFactory()->create();
        $attributes = $this->getFactory()->raw();
        $updater->update(User::factory()->create(), $model, $attributes);

        Event::assertDispatched(($this->modelClass)::getEventName('updating'));

    }

    /**
     * Test if the model can be updated through the web
     *
     * @return void
     */
    public function test_model_can_be_updated_via_the_web()
    {
        $model = $this->getFactory()->create();
        $attributes = $this->getFactory()->raw();
        $this->actAsUser();
        $result = $this->patch(route($this->getRouteName('update'), $model->id), $attributes);
        $result->assertRedirect($this->getRoute('index', true));
        $this->assertAllAttributesAreSetOnModel($attributes);
    }

    /**
     * Test if model can be deleted normally
     *
     * @return void
     */
    public function test_model_can_be_deleted()
    {
        $model = $this->getFactory()->create();
        $model->delete();

        $this->assertEquals(0, ($this->modelClass)::count());
    }

    /**
     * Test if model can be deleted normally
     *
     * @return void
     */
    public function test_model_can_be_deleted_through_action()
    {
        $deleter = app(($this->modelClass)::getContractName('delete'));
        $model = $this->getFactory()->create();
        $deleter->delete(User::factory()->create(), $model);

        $this->assertEquals(0, ($this->modelClass)::count());
    }

    /**
     * Test if model delete fires UpdatingModel event
     *
     * @return void
     */
    public function test_model_delete_fires_event()
    {
        Event::fake();
        $deleter = app(($this->modelClass)::getContractName('delete'));
        $model = $this->getFactory()->create();
        $deleter->delete(User::factory()->create(), $model);

        Event::assertDispatched(($this->modelClass)::getEventName('deleting'));
    }

    /**
     * Test if the model can be deleted through the web
     *
     * @return void
     */
    public function test_model_can_be_deleted_via_the_web()
    {
        $model = $this->getFactory()->create();
        $this->actAsUser();
        $result = $this->delete(route($this->getRouteName('destroy'), $model->id));
        $result->assertRedirect($this->getRoute('index', true));
        $this->assertEquals(0, ($this->modelClass)::count());
    }


}
