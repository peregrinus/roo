<?php
/*
 * ROO :: Religionsunterricht leichtgemacht
 *
 * @package Roo
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/peregrinus/roo
 * @version git: $Id$
 *
 * Roo is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;

class AbstractModel extends Model
{

    use HasFactory;

    public static array $exceptRoutes = [];
    /** @var array[] $validationRules Default validation rules */
    public static array $validationRules = [];
    protected static string $prefix = '';
    protected static string $prefixPlural = '';
    protected $guarded = [];

    public static function getContractName(string $verb): string
    {
        return static::relatedClass('App\\Contracts\\###\\'.ucfirst($verb).'s###s');
    }

    public static function relatedClass(string $pattern): string
    {
        return str_replace('###', static::modelName(), $pattern);
    }

    public static function modelName(): string
    {
        return Str::afterLast(get_called_class(), '\\');
    }

    public static function getEventName(string $verb): string
    {
        return static::relatedClass('App\\Events\\'.ucfirst($verb).'###');
    }

    /**
     * Register the models resource routes
     *
     * @return void
     */
    public static function registerRoutes(): void
    {
        $exceptions = static::$exceptRoutes;
        if (!in_array('index', $exceptions)) {
            $exceptions[] = 'index';
        }

        Route::middleware([
            'auth:sanctum',
            config('jetstream.auth_session'),
            'verified',
        ])->group(function () use ($exceptions) {

            Route::match(['GET', 'HEAD'], '/'.static::$prefixPlural,
                [static::controllerClass(), 'index'])->name(static::pluralKey().'.index');
            Route::resource(static::$prefix, static::controllerClass())
                ->parameters([static::$prefix => 'modelId'])
                ->names(routeNames(static::singularKey()))
                ->except($exceptions);

        });
    }

    public static function controllerClass(): string
    {
        return 'App\\Http\\Controllers\\'.static::modelName().'Controller';
    }

    /**
     * Get the route key in the plural
     *
     * @return string
     */
    public static function pluralKey(): string
    {
        return Pluralizer::plural(self::singularKey());
    }

    /**
     * Get the route key in the singular
     *
     * @return string
     */
    public static function singularKey(): string
    {
        return strtolower(static::modelName());
    }


}
