<?php
/*
 * ROO :: Religionsunterricht leichtgemacht
 *
 * @package Roo
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/peregrinus/roo
 * @version git: $Id$
 *
 * Roo is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Actions\School;

use App\Contracts\School\DeletesSchools;
use App\Events\DeletingSchool;
use App\Models\School;
use App\Models\User;
use Illuminate\Support\Facades\Gate;

class DeleteSchool implements DeletesSchools
{

    /**
     * @return String
     */
    public function redirectTo(): string
    {
        return route('schools.index');
    }

    /**
     * @param  User    $user
     * @param  School  $school
     * @return bool
     */
    public function delete(User $user, School $school): bool
    {
        Gate::forUser($user)->authorize('delete', $school);
        DeletingSchool::dispatch($user, $school);
        return $school->delete();
    }
}
