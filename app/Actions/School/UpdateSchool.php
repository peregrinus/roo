<?php
/*
 * ROO :: Religionsunterricht leichtgemacht
 *
 * @package Roo
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/peregrinus/roo
 * @version git: $Id$
 *
 * Roo is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Actions\School;

use App\Contracts\School\UpdatesSchools;
use App\Events\UpdatingSchool;
use App\Models\School;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UpdateSchool implements UpdatesSchools
{

    /**
     * @return string
     */
    public function redirectTo(): string
    {
        return route('schools.index');
    }

    /**
     * @param  User    $user
     * @param  School  $school
     * @param  array   $input
     * @return bool Success
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function update(User $user, School $school, array $input): bool
    {
        Gate::forUser($user)->authorize('update', $school);
        Validator::make($input, School::$validationRules)->validateWithBag('updateSchool');
        $result = $school->update($input);
        UpdatingSchool::dispatch($user, $school);
        return $result;
    }
}
