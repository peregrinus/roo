<?php
/*
 * ROO :: Religionsunterricht leichtgemacht
 *
 * @package Roo
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/peregrinus/roo
 * @version git: $Id$
 *
 * Roo is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Laravel\Jetstream\Jetstream;
use Laravel\Jetstream\RedirectsActions;

class AbstractCRUDController extends Controller
{
    use RedirectsActions;

    /** @var string $modelClass */
    protected string $modelClass;

    public function __construct()
    {
        $this->modelClass = 'App\\Models\\'.$this->modelName();
    }

    protected function modelName(): string
    {
        return str_replace('Controller', '', Str::afterLast(get_class($this), '\\'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Inertia\Response
     */
    public function index(Request $request)
    {
        Gate::authorize('viewAny', $this->modelClass);
        $records = ($this->modelClass)::all();
        return Jetstream::inertia()->render($request, $this->vuePath('index'), [
            ($this->modelClass)::pluralKey() => $records,
        ]);
    }

    protected function vuePath(string $page): string
    {
        return ucfirst(($this->modelClass)::pluralKey()).'/'.ucfirst($page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        Gate::authorize('create', $this->modelClass);
        $data = $request->validate(($this->modelClass)::$validationRules);
        $creator = app(($this->modelClass)::getContractName('create'));
        $creator->create($request->user(), $data);
        return $this->redirectPath($creator);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Inertia\Response
     */
    public function create(Request $request)
    {
        Gate::authorize('create', $this->modelClass);
        return Jetstream::inertia()->render($request, $this->vuePath('editor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Inertia\Response
     */
    public function edit(Request $request, $modelId)
    {
        $model = ($this->modelClass)::findOrFail($modelId);
        Gate::authorize('update', $model);
        return Jetstream::inertia()->render($request, $this->vuePath('editor'), [
            ($this->modelClass)::singularKey() => $model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(Request $request, $modelId)
    {
        $model = ($this->modelClass)::findOrFail($modelId);
        Gate::authorize('update', $model);
        $data = $request->validate(($this->modelClass)::$validationRules);
        $updater = app(($this->modelClass)::getContractName('update'));
        $updater->update($request->user(), $model, $data);
        return $this->redirectPath($updater);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy(Request $request, $modelId)
    {
        $model = ($this->modelClass)::findOrFail($modelId);
        Gate::authorize('delete', $model);
        $deleter = app(($this->modelClass)::getContractName('delete'));
        $deleter->delete($request->user(), $model);
        return $this->redirectPath($deleter);
    }


}
