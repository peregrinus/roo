/*
 * *
 *  * ROO :: Religionsunterricht leichtgemacht
 *  *
 *  * @package Roo
 *  * @author Christoph Fischer <chris@toph.de>
 *  * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 *  * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 *  * @link https://codeberg.org/peregrinus/roo
 *  * @version git: $Id$
 *  *
 *  * Roo is based on the Laravel framework (https://laravel.com).
 *  * This file may contain code created by Laravel's scaffolding functions.
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

export const chartColors = {
    default: {
        primary: '#00D1B2',
        info: '#209CEE',
        danger: '#FF3860',
    },
}

const randomChartData = (n) => {
    const data = []

    for (let i = 0; i < n; i++) {
        data.push(Math.round(Math.random() * 200))
    }

    return data
}

const datasetObject = (color, points) => {
    return {
        fill: false,
        borderColor: chartColors.default[color],
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: chartColors.default[color],
        pointBorderColor: 'rgba(255,255,255,0)',
        pointHoverBackgroundColor: chartColors.default[color],
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 4,
        data: randomChartData(points),
        tension: 0.5,
        cubicInterpolationMode: 'default',
    }
}

export const sampleChartData = (points = 9) => {
    const labels = []

    for (let i = 1; i <= points; i++) {
        labels.push(`0${i}`)
    }

    return {
        labels,
        datasets: [
            datasetObject('primary', points),
            datasetObject('info', points),
            datasetObject('danger', points),
        ],
    }
}
