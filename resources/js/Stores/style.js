/*
 * *
 *  * ROO :: Religionsunterricht leichtgemacht
 *  *
 *  * @package Roo
 *  * @author Christoph Fischer <chris@toph.de>
 *  * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 *  * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 *  * @link https://codeberg.org/peregrinus/roo
 *  * @version git: $Id$
 *  *
 *  * Roo is based on the Laravel framework (https://laravel.com).
 *  * This file may contain code created by Laravel's scaffolding functions.
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

import { defineStore } from 'pinia'
import * as styles from '@/styles'
import { darkModeKey, styleKey } from '@/config'

export const useStyleStore = defineStore('style', {
    state: () => ({
        /* Styles */
        asideStyle: '',
        asideScrollbarsStyle: '',
        asideBrandStyle: '',
        asideMenuItemStyle: '',
        asideMenuItemActiveStyle: '',
        asideMenuDropdownStyle: '',
        navBarItemLabelStyle: '',
        navBarItemLabelHoverStyle: '',
        navBarItemLabelActiveColorStyle: '',
        overlayStyle: '',

        /* Dark mode */
        darkMode: false,
    }),
    actions: {
        setStyle (payload) {
            if (!styles[payload]) {
                return
            }

            if (typeof localStorage !== 'undefined') {
                localStorage.setItem(styleKey, payload)
            }

            const style = styles[payload]

            for (const key in style) {
                this[`${key}Style`] = style[key]
            }
        },

        setDarkMode (payload = null) {
            this.darkMode = payload !== null ? payload : !this.darkMode

            if (typeof localStorage !== 'undefined') {
                localStorage.setItem(darkModeKey, this.darkMode ? '1' : '0')
            }

            if (typeof document !== 'undefined') {
                document.body.classList[this.darkMode ? 'add' : 'remove'](
                    'dark-scrollbars',
                )

                document.documentElement.classList[this.darkMode ? 'add' : 'remove'](
                    'dark-scrollbars-compat',
                )
            }
        },
    },
})
