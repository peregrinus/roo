/*
 * *
 *  * ROO :: Religionsunterricht leichtgemacht
 *  *
 *  * @package Roo
 *  * @author Christoph Fischer <chris@toph.de>
 *  * @copyright (c) Christoph Fischer, https://christoph-fischer.de
 *  * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 *  * @link https://codeberg.org/peregrinus/roo
 *  * @version git: $Id$
 *  *
 *  * Roo is based on the Laravel framework (https://laravel.com).
 *  * This file may contain code created by Laravel's scaffolding functions.
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

export const basic = {
    aside: 'bg-gray-800',
    asideScrollbars: 'aside-scrollbars-gray',
    asideBrand: 'bg-gray-900 text-white',
    asideMenuItem: 'text-gray-300 hover:text-white',
    asideMenuItemActive: 'font-bold text-white',
    asideMenuDropdown: 'bg-gray-700/50',
    navBarItemLabel: 'text-black',
    navBarItemLabelHover: 'hover:text-blue-500',
    navBarItemLabelActiveColor: 'text-blue-600',
    overlay: 'from-gray-700 via-gray-900 to-gray-700',
}

export const white = {
    aside: 'bg-white',
    asideScrollbars: 'aside-scrollbars-light',
    asideBrand: '',
    asideMenuItem: 'text-blue-600 hover:text-black dark:text-white',
    asideMenuItemActive: 'font-bold text-black dark:text-white',
    asideMenuDropdown: 'bg-gray-100/75',
    navBarItemLabel: 'text-blue-600',
    navBarItemLabelHover: 'hover:text-black',
    navBarItemLabelActiveColor: 'text-black',
    overlay: 'from-white via-gray-100 to-white',
}
